import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'exception.dart';

class ApiClient {
  final String baseApiUrl;

  ApiClient({required this.baseApiUrl});

  Future<dynamic> get(
    String url, {
    Map<String, String>? addHeaders,
    Map<String, String>? params,
  }) async {
    dynamic responseJson;
    try {
      final uri = _buildUrl(url, params: params);
      final response = await http.get(uri);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No internet conection!');
    }
    return responseJson;
  }

  Uri _buildUrl(String url, {Map<String, String>? params}) {
    Uri uri;
    if (params != null) {
      uri = Uri.https(baseApiUrl, url, params);
    } else {
      uri = Uri.https(baseApiUrl, url);
    }
    return uri;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        final responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}

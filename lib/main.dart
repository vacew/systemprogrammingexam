import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:math_quiz_based_on_provider/app_config.dart';
import 'package:math_quiz_based_on_provider/localization/translate_preference.dart';
import 'package:math_quiz_based_on_provider/utils/logger.dart';
import 'package:math_quiz_based_on_provider/view_models/theme_view_model.dart';
import 'package:provider/provider.dart';

import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final themeViewModel = ThemeViewModel();
  await themeViewModel.init();

  setupLogger();

  const configuredApp =
      AppConfig(appName: 'Math Quiz', apiUrl: 'run.mocky.io', child: App());

  final delegate = await LocalizationDelegate.create(
      fallbackLocale: 'en',
      preferences: TranslatePreferences(),
      supportedLocales: ['en', 'uk']);

  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

  runApp(LocalizedApp(
      delegate,
      ChangeNotifierProvider.value(
        value: themeViewModel,
        child: configuredApp,
      )));
}

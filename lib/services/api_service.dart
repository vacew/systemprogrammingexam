import 'package:math_quiz_based_on_provider/models/models.dart';
import 'package:math_quiz_based_on_provider/network/api_client.dart';

const String oneAnswerPath = '/v3/0aeb7418-0c94-41ce-8f5b-09889cf37a58';
const String trueFalsePath = '/v3/5d4b54e3-7172-43d0-b924-fe5d51707e8f';

class ApiService {
  late ApiClient _apiClient;

  ApiService(String baseApiUrl) {
    _apiClient = ApiClient(baseApiUrl: baseApiUrl);
  }

  Future<List<OneAnswerModel>> fetchOneAnswerData() async {
    try {
      final response = await _apiClient.get(oneAnswerPath);
      return List<OneAnswerModel>.from(
          response.map((json) => OneAnswerModel.fromJson(json)));
    } catch (e) {
      rethrow;
    }
  }
  Future<List<TrueFalseModel>> fetchTrueFalseData() async {
    try {
      final response = await _apiClient.get(trueFalsePath);
      return List<TrueFalseModel>.from(
          response.map((json) => TrueFalseModel.fromJson(json)));
    } catch (e) {
      rethrow;
    }
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


const Color redLight = Color(0xFFE44125);
const Color alertColor = Color(0xFFEB5252);
const Color blackShade = Color(0xFF222222);
const Color cherryRed = Color(0xffe8001d);
const Color greyShadeLight = Color(0xFFF0F2F5);
const Color greyLight = Color(0xFF89898F);
const Color greyDark = Color(0xFF797979);
const Color greyDarkTitles = Color(0xFF6C6C6C);
const Color mainBlue = Color(0xFF005599);
const Color mainBackground = Color(0xFFF0F2F5);
const Color blackText = Color(0xFF444444);
const Color lightText = Color(0xFFFAFAFA);
const Color accentBlue = Color(0xFF1884DB);
const Color blackColor = Color(0xFF212121);
const Color greyBorder = Color(0xFFCCCCCC);
const Color darkBackground = Color(0xFF121212);
const Color whiteColor = Color(0xFFFFFFFF);
const Color primaryColorLight = Color(0xFFFCFCFC);
const Color primaryColorDark = Color(0xFF2E2E2E);
const Color greenColor = Color(0xFF67C070);
const Color dividerColor = Color(0xFFECECEC);
const Color yellowColor = Color(0xFFF1943E);
const Color yellowProgressBar = Color(0xFFFFAE00);
const Color commentFormBg = Color(0xFFE6F1FA);
const Color shopInfoBackgroundColor = Color(0xFFE5E5E5);
const Color tagGrey = Color(0xE0E0E0FF);
const Color successSnackBarColor = Color(0xFF2C8730);
const Color lightBlue = Color(0xFF8BCFE7);
const Color mainPurple = Color(0xFF673AB7);

ThemeData darkTheme = ThemeData(
  highlightColor: redLight,
  splashColor: redLight,
  primaryColor: Colors.black,
  backgroundColor: blackShade,
  colorScheme:
  const ColorScheme.light(primary: lightText, secondary: lightText, brightness: Brightness.dark),
  floatingActionButtonTheme:
      const FloatingActionButtonThemeData(foregroundColor: Colors.black),
  dividerColor: Colors.black12,
  // or use string of the font in the assets 'SFUIDisplay'
  fontFamily: GoogleFonts.roboto().fontFamily,
  textTheme: const TextTheme(
    headline1:
    TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: whiteColor),
    headline2:
    TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: whiteColor),
    headline4: TextStyle(
        fontSize: 17.0, fontWeight: FontWeight.w500, color: whiteColor),
    headline6: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
    bodyText1: TextStyle(
        fontSize: 16.0, fontWeight: FontWeight.w400, color: whiteColor),
    bodyText2: TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w400,
        color:
        whiteColor), // or use string of the font in the assets 'SFUIDisplay'
  ),
);

ThemeData lightTheme = ThemeData(
  highlightColor: redLight,
  splashColor: redLight,
  primaryColor: Colors.white,
  backgroundColor: greyShadeLight,
  colorScheme:
  const ColorScheme.light(primary: blackText, secondary: blackText, brightness: Brightness.light),
  floatingActionButtonTheme:
      const FloatingActionButtonThemeData(foregroundColor: Colors.white),
  dividerColor: Colors.white54,
  // or use string of the font in the assets 'SFUIDisplay'
  fontFamily: GoogleFonts.roboto().fontFamily,
  textTheme: const TextTheme(
    headline1: TextStyle(
        fontSize: 24.0, fontWeight: FontWeight.w500, color: blackColor),
    headline2: TextStyle(
        fontSize: 20.0, fontWeight: FontWeight.w500, color: blackColor),
    headline4: TextStyle(
        fontSize: 17.0, fontWeight: FontWeight.w500, color: blackColor),
    headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    bodyText1: TextStyle(
        fontSize: 16.0, fontWeight: FontWeight.w400, color: blackColor),
    bodyText2: TextStyle(
        fontSize: 12.0, fontWeight: FontWeight.w400, color: blackColor), //
    // or use string of the font in the assets 'SFUIDisplay'
  ),
);

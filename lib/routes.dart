import 'package:flutter/material.dart';

import 'package:math_quiz_based_on_provider/views/home_page.dart';
import 'package:math_quiz_based_on_provider/views/settings_screen.dart';
import 'package:math_quiz_based_on_provider/views/splash_screen.dart';

const String homeScreenRoute = '/home_screen';
const String splashScreenRoute = '/splash_screen';
const String settingsScreenRoute = '/settings_screen';

Map<String, WidgetBuilder> applicationRoutes = <String, WidgetBuilder>{
  splashScreenRoute: (context) => const SplashScreen(),
  homeScreenRoute: (context) => const HomePage(),
  settingsScreenRoute: (context) => const SettingsScreen(),
};

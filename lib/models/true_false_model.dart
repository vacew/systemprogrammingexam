class TrueFalseModel {
  int? id;
  String? question;
  bool rightAnswer;

  TrueFalseModel({required this.rightAnswer, this.id, this.question, });

  factory TrueFalseModel.fromJson(Map<String, dynamic> json) => TrueFalseModel(
      id: json['id'] == null ? null : json['id'] as int?,
      question: json['question'] == null ? null : json['question'] as String?,
      rightAnswer: json['rightAnswer'] ?? false);

  Map<String, dynamic> toJson() => {
        'id': id,
        'question': question,
        'rightAnswer': rightAnswer,
      };

  @override
  String toString() {
    return 'TrueFalseModel{id: $id, question: $question, rightAnswer: $rightAnswer}';
  }
}

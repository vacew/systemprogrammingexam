class Item {
  String? key;
  String? value;

  Item({this.key, this.value});

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        key: json['key'] as String?,
        value: json['value'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'key': key,
        'value': value,
      };

  @override
  String toString() {
    return 'Item{key: $key, value: $value}';
  }
}

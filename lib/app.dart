import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:math_quiz_based_on_provider/app_config.dart';
import 'package:math_quiz_based_on_provider/routes.dart';
import 'package:math_quiz_based_on_provider/services/api_service.dart';
import 'package:math_quiz_based_on_provider/view_models/theme_view_model.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final localizationDelegate = LocalizedApp.of(context).delegate;
    final appConfig = AppConfig.of(context);

    return LocalizationProvider(
        state: LocalizationProvider.of(context).state,
        child: Provider(
            create: (_) => ApiService(appConfig!.apiUrl),
            child:
                Consumer<ThemeViewModel>(builder: (context, themeViewModel, _) {
              return MaterialApp(
                title: 'Quiz App',
                debugShowCheckedModeBanner: false,
                theme: themeViewModel.getThemeData,
                darkTheme: themeViewModel.currentDarkTheme,
                initialRoute: splashScreenRoute,
                localizationsDelegates: [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  localizationDelegate
                ],
                supportedLocales: localizationDelegate.supportedLocales,
                locale: localizationDelegate.currentLocale,
                routes: applicationRoutes,
              );
            })));
  }
}

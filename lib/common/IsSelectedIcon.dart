import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';

class IsSelectedIcon extends StatelessWidget {
  const IsSelectedIcon({
    Key? key,
    required this.isSelected,
  }) : super(key: key);

  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24,
      height: 24,
      decoration: BoxDecoration(
        color: isSelected ? Theme.of(context).cardColor : Colors.transparent,
        border: Border.all(
          color: greyBorder,
          width: 1,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(50),
        ),
      ),
      child: isSelected
          ? const Icon(
              Icons.circle,
              size: 15,
              color: mainPurple,
            )
          : const SizedBox(),
    );
  }
}

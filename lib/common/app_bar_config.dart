import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';
import 'package:math_quiz_based_on_provider/routes.dart';
AppBar defaultAppBar(BuildContext context,
    {required String title,  bool isActionsNeeded = false}) {
  return AppBar(
    backgroundColor: Colors.deepPurple,
    iconTheme: const IconThemeData(color: whiteColor),
    title: Text(title, style: Theme.of(context).textTheme.headline2!.copyWith(color: whiteColor),),
    actions: [
      if(isActionsNeeded)
      IconButton(onPressed: () {
        Navigator.pushNamed(context, settingsScreenRoute);
      }, icon: const Icon(Icons.settings))
    ],
  );
}

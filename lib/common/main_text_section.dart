import 'package:flutter/material.dart';

class MainTextWidget extends StatelessWidget {
  final String text;
  final int heightRatio;

  const MainTextWidget({Key? key, required this.text, this.heightRatio = 2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / heightRatio,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text,
              style: Theme.of(context)
                  .textTheme
                  .headline1!
                  .copyWith(fontSize: 30)),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width / 6,
              child: const Divider(color: Colors.deepPurple, thickness: 3),
            ),
          )
        ],
      ),
    );
  }
}

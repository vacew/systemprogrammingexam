import 'package:flutter/material.dart';

TextStyle defaultTextStyle(BuildContext context) {
  return const TextStyle(
      color: Colors.deepPurple,
      fontWeight: FontWeight.bold,
      fontSize: 25);
}

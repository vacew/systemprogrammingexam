import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:math_quiz_based_on_provider/common/IsSelectedIcon.dart';
import 'package:math_quiz_based_on_provider/view_models/theme_view_model.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';

class ModeItem extends StatelessWidget {
  final BrightnessThemeModel themeModel;
  final String themeName;
  final bool isSelected;
  final Function() onItemTap;

  const ModeItem(
      {Key? key,
      required this.themeModel,
      required this.onItemTap,
      required this.themeName,
      required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onItemTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 7,
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(.05),
                    blurRadius: 5.0, // soften the shadow
                    spreadRadius: 0.0, //extend the shadow
                    offset: const Offset(
                      0.0, // Move to right 10  horizontally
                      2.0, // Move to bottom 10 Vertically
                    ),
                  )
                ],
              ),
              child: Card(
                elevation: 0,
                color: Theme.of(context).cardColor,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: isSelected ? accentBlue : Colors.transparent,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          AnimatedDefaultTextStyle(
                            duration: const Duration(milliseconds: 300),
                            style: isSelected
                                ? Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: accentBlue,
                                      fontWeight: FontWeight.w400,
                                    )
                                : Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                    ),
                            child: Text(
                              themeName,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                        right: 20,
                        top: 20,
                        child: IsSelectedIcon(isSelected: isSelected)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

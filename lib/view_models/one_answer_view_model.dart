import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/models/one_answer_model.dart';
import 'package:math_quiz_based_on_provider/network/response.dart';
import 'package:math_quiz_based_on_provider/services/api_service.dart';

class OneAnswerViewModel extends ChangeNotifier {
  int _questionNumber = 0;
  int _score = 0;
  bool _isFinalQuestion = false;
  int _selectedValue = -1;
  bool isAnswerSelected = false;

  final quiz = OneAnswerModel(
      id: 0,
      question: '7 + 4',
      answers: ['13', '14', '11', '9'],
      rightAnswer: '11');

  int get selectedValue => _selectedValue;

  set selectedValue(int value) {
    _selectedValue = value;
    notifyListeners();
  }

  final ApiService _oneAnswerApiService;
  NetworkResponse<List<OneAnswerModel>> oneAnswerListResponse =
      NetworkResponse.none();

  OneAnswerViewModel(this._oneAnswerApiService);

  Future<void> fetchQuiz() async {
    oneAnswerListResponse =
        NetworkResponse<List<OneAnswerModel>>.loading('Fetching users...');
    notifyListeners();

    try {
      final oneAnswerList = await _oneAnswerApiService.fetchOneAnswerData();
      oneAnswerListResponse = NetworkResponse.completed(oneAnswerList);
    } catch (e) {
      oneAnswerListResponse = NetworkResponse.error(e.toString());
    } finally {
      notifyListeners();
    }
  }

  int get questionNumber => _questionNumber;

  int get score => _score;

  void incrementScore() {
    _score++;
    notifyListeners();
  }

  void nextQuestion() {
    if (_questionNumber < [quiz].length - 1) {
      _questionNumber += 1;
    } else {
      _isFinalQuestion = true;
    }
    notifyListeners();
  }

  void checkAnswer({required int index, required Function() onFinalQuestion}) {
    if (quiz.answers![index] == quiz.rightAnswer) {
      incrementScore();
    }
    nextQuestion();
    if (_isFinalQuestion) {
      onFinalQuestion();
    }
    notifyListeners();
  }
}

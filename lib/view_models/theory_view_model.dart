import 'package:flutter/material.dart';

class TheoryViewModel extends ChangeNotifier {
  final List<String> theoryList = [
    'Basics of algebra',
    'Adding, subtracting, and multiplying algebraic expressions',
    'Simplification of algebraic equations, using the BODMAS rule',
    'Substitution',
    'Solving inequalities of equations',
    'Exponents',
    'Surds',
    'Square roots',
    'Cube roots',
    'Laws of exponents',
    'Simplifying equations',
    'Algebraic properties (associative properties, distributive laws, communicative laws)',
    'Cross multiplication',
    'Polynomials',
    'Adding, subtracting, multiplying, and dividing polynomials',
    'Rational expressions',
    'Long division of polynomials',
    'Conjugate',
    'Rationalizing denominator',
    'Quadratic equations',
  ];
}

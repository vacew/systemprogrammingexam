import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/database/home_dao.dart';
import 'package:math_quiz_based_on_provider/models/item_model.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';

enum BrightnessThemeModel { light, dark, auto }

class ThemeViewModel extends ChangeNotifier {
  final HomeDao homeDao = HomeDao();
  bool isLightTheme = true;
  ThemeData? currentDarkTheme;
  late BrightnessThemeModel currentThemeType;

  Future<void> init() async {
    final theme = await homeDao.getBrightnessSavedSettings();
    if (theme == null || theme == 'auto') {
      theme ??
          await homeDao
              .save(Item(key: homeDao.appColorThemeKey, value: 'auto'));
      currentThemeType = BrightnessThemeModel.auto;
      currentDarkTheme = darkTheme;
    } else {
      if (theme == 'light') {
        currentThemeType = BrightnessThemeModel.light;
      } else {
        currentThemeType = BrightnessThemeModel.dark;
        currentDarkTheme = darkTheme;
        isLightTheme = false;
      }
    }
  }

  ThemeData get getThemeData => isLightTheme ? lightTheme : darkTheme;

  void selectSystemTheme() {
    homeDao.save(Item(key: homeDao.appColorThemeKey, value: 'auto'));
    currentThemeType = BrightnessThemeModel.auto;
    currentDarkTheme = darkTheme;
    isLightTheme = true;
    notifyListeners();
  }

  void selectLightTheme() {
    homeDao.save(Item(key: homeDao.appColorThemeKey, value: 'light'));
    currentThemeType = BrightnessThemeModel.light;
    currentDarkTheme = null;
    isLightTheme = true;
    notifyListeners();
  }

  void selectDarkTheme() {
    homeDao.save(Item(key: homeDao.appColorThemeKey, value: 'dark'));
    currentThemeType = BrightnessThemeModel.dark;
    currentDarkTheme = darkTheme;
    isLightTheme = false;
    notifyListeners();
  }
}

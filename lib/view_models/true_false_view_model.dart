import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/models/true_false_model.dart';
import 'package:math_quiz_based_on_provider/network/response.dart';
import 'package:math_quiz_based_on_provider/services/api_service.dart';

class TrueFalseViewModel extends ChangeNotifier {
  final ApiService _apiService;
  int _questionNumber = 0;
  int _score = 0;
  bool _isFinalQuestion = false;
  int _selectedValue = -1;
  bool isAnswerSelected = false;

  final quiz =
      TrueFalseModel(id: 0, question: 'Is 7 + 4 = 11', rightAnswer: true);

  int get selectedValue => _selectedValue;

  set selectedValue(int value) {
    _selectedValue = value;
    notifyListeners();
  }

  NetworkResponse<List<TrueFalseModel>> trueFalseListResponse =
      NetworkResponse.none();

  TrueFalseViewModel(this._apiService);

  Future<void> fetchQuiz() async {
    trueFalseListResponse =
        NetworkResponse<List<TrueFalseModel>>.loading('Fetching users...');
    notifyListeners();

    try {
      final trueFalseList = await _apiService.fetchTrueFalseData();
      trueFalseListResponse = NetworkResponse.completed(trueFalseList);
    } catch (e) {
      trueFalseListResponse = NetworkResponse.error(e.toString());
    } finally {
      notifyListeners();
    }
  }

  int get questionNumber => _questionNumber;

  int get score => _score;

  void incrementScore() {
    _score++;
    notifyListeners();
  }

  void nextQuestion() {
    if (_questionNumber < [quiz].length - 1) {
      _questionNumber += 1;
    } else {
      _isFinalQuestion = true;
    }
    notifyListeners();
  }

  void checkAnswer(bool answer, Function() onFinalQuestion) {
    if (answer == quiz.rightAnswer) {
      incrementScore();
    }
    nextQuestion();
    if (_isFinalQuestion) {
      onFinalQuestion();
    }
    notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';

class ResultPage extends StatelessWidget {
  final int count;

  const ResultPage({Key? key, required this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(
        context,
        title: 'Result Page',
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MainTextWidget(
              text: 'Your score is: $count',
            ),
            Card(
              child: ListTile(
                onTap: () {
                  Navigator.popUntil(context, (route) => route.isFirst);
                },
                title: const Text('Home Page'),
                leading: const Icon(Icons.arrow_back_ios_sharp),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

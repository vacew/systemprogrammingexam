import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';

import 'one_answer_page.dart';
import 'true_false_page.dart';

class QuizzesPage extends StatelessWidget {
  const QuizzesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(context, title: 'Quizzes'),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const MainTextWidget(
              text: 'Select type of quiz: ',
            ),
            Column(
              children: [
                Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const TrueFalsePage(),
                        ),
                      );
                    },
                    title: const Text('True False Quiz'),
                    trailing: const Icon(Icons.arrow_forward_ios_sharp),
                  ),
                ),
                Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const OneAnswerPage(),
                        ),
                      );
                    },
                    title: const Text('One Answer Quiz'),
                    trailing: const Icon(Icons.arrow_forward_ios_sharp),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';
import 'package:math_quiz_based_on_provider/view_models/theory_view_model.dart';
import 'package:math_quiz_based_on_provider/views/quizzes_page.dart';
import 'package:math_quiz_based_on_provider/views/theory_page.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(context, title: 'Quizzes', isActionsNeeded: true),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const MainTextWidget(
              text: 'Select type of activity: ',
            ),
            Column(
              children: [
                Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const QuizzesPage(),
                        ),
                      );
                    },
                    title: const Text('Quizzes'),
                    trailing: const Icon(Icons.arrow_forward_ios_sharp),
                  ),
                ),
                Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChangeNotifierProvider(
                              child: const TheoryPage(),
                              create: (_) => TheoryViewModel()),
                        ),
                      );
                    },
                    title: const Text('Theory'),
                    trailing: const Icon(Icons.arrow_forward_ios_sharp),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

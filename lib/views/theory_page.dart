import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';
import 'package:math_quiz_based_on_provider/view_models/theory_view_model.dart';
import 'package:provider/provider.dart';

class TheoryPage extends StatelessWidget {
  const TheoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final topics = context.read<TheoryViewModel>().theoryList;
    return Scaffold(
        appBar: defaultAppBar(context, title: 'Theory'),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MainTextWidget(
                  text: 'Select your topic: ',
                  heightRatio: 10,
                ),
                ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: topics.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        onTap: () {},
                        title: Text(topics[index]),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ));
  }
}

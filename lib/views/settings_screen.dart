import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/mode_item.dart';
import 'package:math_quiz_based_on_provider/view_models/theme_view_model.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    final themeViewModel = context.watch<ThemeViewModel>();
    return Scaffold(
      appBar: defaultAppBar(context, title: 'Settings'),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              const SizedBox(
                height: 24,
              ),
              ModeItem(
                themeName: 'System style',
                themeModel: BrightnessThemeModel.auto,
                isSelected: BrightnessThemeModel.auto ==
                    themeViewModel.currentThemeType,
                onItemTap: () {
                  themeViewModel.selectSystemTheme();
                },
              ),
              ModeItem(
                themeName: 'Light style',
                themeModel: BrightnessThemeModel.light,
                isSelected: BrightnessThemeModel.light ==
                    themeViewModel.currentThemeType,
                onItemTap: () {
                  themeViewModel.selectLightTheme();
                },
              ),
              ModeItem(
                themeName: 'Dark style',
                themeModel: BrightnessThemeModel.dark,
                isSelected: BrightnessThemeModel.dark ==
                    themeViewModel.currentThemeType,
                onItemTap: () {
                  themeViewModel.selectDarkTheme();
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}

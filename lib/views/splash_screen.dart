import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:math_quiz_based_on_provider/routes.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((timeStamp) async {
      Future.delayed(
        const Duration(seconds: 3),
            () => Navigator.pushReplacementNamed(context, homeScreenRoute),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainPurple,
      body: Center(
        child: Container(
            alignment: Alignment.center,
            height: 175,
            width: 175,
            margin: const EdgeInsets.all(100.0),
            decoration: const BoxDecoration(
              color: whiteColor,
              shape: BoxShape.circle,
            ),
            child: Text('Easy\nMath',
                style: Theme
                    .of(context)
                    .textTheme
                    .headline1!
                    .copyWith(color: blackShade,
                    fontWeight: FontWeight.bold, fontStyle: FontStyle.italic))),
      ),
    );
  }
}

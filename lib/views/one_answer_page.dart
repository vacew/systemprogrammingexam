import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/IsSelectedIcon.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';
import 'package:math_quiz_based_on_provider/models/models.dart';
import 'package:math_quiz_based_on_provider/network/response.dart';
import 'package:math_quiz_based_on_provider/services/api_service.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';
import 'package:math_quiz_based_on_provider/view_models/one_answer_view_model.dart';
import 'package:math_quiz_based_on_provider/views/result_page.dart';
import 'package:provider/provider.dart';

class OneAnswerPage extends StatelessWidget {
  const OneAnswerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => OneAnswerViewModel(context.read<ApiService>()),
        child: const QuestionItem());
  }
}

class QuestionItem extends StatefulWidget {
  const QuestionItem({Key? key}) : super(key: key);

  @override
  _QuestionItemState createState() => _QuestionItemState();
}

class _QuestionItemState extends State<QuestionItem> {
  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      context.read<OneAnswerViewModel>().fetchQuiz();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final oneQuestionProvider = context.watch<OneAnswerViewModel>();
    final quizzes = [oneQuestionProvider.quiz];
    return Scaffold(
        appBar: defaultAppBar(context, title: 'One Answer Quiz'),
        body: oneQuestionProvider.oneAnswerListResponse.status.isCompleted
            ? PageContent(quizzes: quizzes)
            : const Center(
                child: CircularProgressIndicator(
                color: mainPurple,
              )));
  }
}

class PageContent extends StatelessWidget {
  final List<OneAnswerModel> quizzes;

  const PageContent({Key? key, required this.quizzes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final oneAnswerViewModel = context.watch<OneAnswerViewModel>();
    final quizIndex = oneAnswerViewModel.questionNumber;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                MainTextWidget(
                  text: quizzes[quizIndex].question!,
                  heightRatio: 3,
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: quizzes[quizIndex].answers!.length,
                  itemBuilder: (context, index) {
                    return SlideInRight(
                        delay: Duration(milliseconds: index * 100),
                        child: Card(
                          child: ListTile(
                            onTap: () {
                              oneAnswerViewModel.selectedValue = index;
                              oneAnswerViewModel.isAnswerSelected = true;
                            },
                            title: Text(quizzes[quizIndex].answers![index]),
                            leading: IsSelectedIcon(
                                isSelected:
                                    oneAnswerViewModel.selectedValue == index),
                          ),
                        ));
                  },
                ),
              ],
            ),
          ),
        ),
        oneAnswerViewModel.isAnswerSelected
            ? InkWell(
                onTap: () {
                  oneAnswerViewModel.checkAnswer(
                      index: oneAnswerViewModel.selectedValue,
                      onFinalQuestion: () => Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return ResultPage(
                                count: oneAnswerViewModel.score,
                              );
                            }),
                          ));
                  oneAnswerViewModel.isAnswerSelected = false;
                  oneAnswerViewModel.selectedValue = -1;
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color: mainPurple,
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Confirm',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(color: whiteColor, fontSize: 20),
                        ),
                      ],
                    ),
                    trailing: const Icon(
                      Icons.arrow_forward,
                      color: whiteColor,
                    ),
                  ),
                ),
              )
            : const SizedBox(),
        // TweenAnimationBuilder(
        //   builder: (BuildContext context, double value, Widget? child) {
        //     return Opacity(
        //       opacity: value,
        //       child: child,
        //     );
        //   },
        //   duration: const Duration(milliseconds: 500),
        //   tween: Tween<double>(begin: 1, end: 0),
        //   child: AnimatedContainer(
        //       width: oneAnswerViewModel.isAnswerSelected
        //           ? MediaQuery.of(context).size.width
        //           : (MediaQuery.of(context).size.width / quizzes.length) *
        //               quizIndex,
        //       duration: Duration(
        //           milliseconds: oneAnswerViewModel.isAnswerSelected ? 500 : 0),
        //       child: const Divider(color: Colors.deepPurple, thickness: 3)),
        // ),
      ],
    );
  }
}

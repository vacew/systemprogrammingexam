import 'package:flutter/material.dart';
import 'package:math_quiz_based_on_provider/common/IsSelectedIcon.dart';
import 'package:math_quiz_based_on_provider/common/app_bar_config.dart';
import 'package:math_quiz_based_on_provider/common/main_text_section.dart';
import 'package:math_quiz_based_on_provider/models/models.dart';
import 'package:math_quiz_based_on_provider/network/response.dart';
import 'package:math_quiz_based_on_provider/services/api_service.dart';
import 'package:math_quiz_based_on_provider/theme/theme.dart';
import 'package:math_quiz_based_on_provider/view_models/true_false_view_model.dart';
import 'package:math_quiz_based_on_provider/views/result_page.dart';
import 'package:provider/provider.dart';

class TrueFalsePage extends StatelessWidget {
  const TrueFalsePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => TrueFalseViewModel(context.read<ApiService>()),
        child: const QuestionItem());
  }
}

class QuestionItem extends StatefulWidget {
  const QuestionItem({Key? key}) : super(key: key);

  @override
  _QuestionItemState createState() => _QuestionItemState();
}

class _QuestionItemState extends State<QuestionItem> {
  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await context.read<TrueFalseViewModel>().fetchQuiz();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final trueFalseProvider = context.watch<TrueFalseViewModel>();
    return Scaffold(
      appBar: defaultAppBar(context, title: 'True False Page'),
      body: trueFalseProvider.trueFalseListResponse.status.isCompleted
          ? PageContent(quizzes: [trueFalseProvider.quiz])
          : const Center(
              child: CircularProgressIndicator(
              color: mainPurple,
            )),
    );
  }
}

class PageContent extends StatelessWidget {
  final List<TrueFalseModel> quizzes;

  const PageContent({Key? key, required this.quizzes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final trueFalseViewModel = context.watch<TrueFalseViewModel>();
    final quizIndex = trueFalseViewModel.questionNumber;

    return Column(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: [
                MainTextWidget(
                  text: quizzes[quizIndex].question!,
                  heightRatio: 3,
                ),
                Card(
                  child: ListTile(
                    onTap: () {
                      trueFalseViewModel.selectedValue = 0;
                      trueFalseViewModel.isAnswerSelected = true;
                    },
                    title: const Text('True'),
                    leading: IsSelectedIcon(
                        isSelected: trueFalseViewModel.selectedValue == 0),
                  ),
                ),
                Card(
                  child: ListTile(
                    onTap: () {
                      trueFalseViewModel.selectedValue = 1;
                      trueFalseViewModel.isAnswerSelected = true;
                    },
                    title: const Text('False'),
                    leading: IsSelectedIcon(
                        isSelected: trueFalseViewModel.selectedValue == 1),
                  ),
                ),
              ],
            ),
          ),
        ),
        trueFalseViewModel.isAnswerSelected
            ? InkWell(
                onTap: () {
                  trueFalseViewModel.checkAnswer(
                      trueFalseViewModel.selectedValue == 0 ? true : false,
                      () => Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return ResultPage(
                                count: trueFalseViewModel.score,
                              );
                            }),
                          ));
                  trueFalseViewModel.isAnswerSelected = false;
                  trueFalseViewModel.selectedValue = -1;
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color: mainPurple,
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Confirm',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(color: whiteColor, fontSize: 20),
                        ),
                      ],
                    ),
                    trailing: const Icon(
                      Icons.arrow_forward,
                      color: whiteColor,
                    ),
                  ),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
